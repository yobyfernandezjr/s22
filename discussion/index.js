// console.log("Hi")

// Array methods

/*
	Mutator Methods
		- seeks to modify the contents of an array.
		- are functions that mutate an array after they are created. These methods manipulate original array performing various tasks such as adding or removing elements.
*/

let fruits = ["Apple", "Orange", "Kiwi", "Watermelon"];
/*
	push()
		- adds an element in the end of an array and returns the array's length.

	Syntax:
		arrayName.push(element);
*/
console.log("Current Fruits Array:")
console.log(fruits);

// Adding element using push()
fruits.push("Mango");
console.log(fruits);

let fruitsLength = fruits.push("Melon");
console.log(fruitsLength);
console.log(fruits);

fruits.push("Avocado", "Guava");
console.log(fruits);

/*
	pop()
		- removes the last element in our array and returns the removed element (when applied inside a variable).

	Syntax:
		arrayName.pop();
*/

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("The update list of fruits:")
console.log(fruits);

/*
	unshift()

		- adds one or more elements at the beginning of an array
		- returns the length of the array (when presented inside a variable)

	Syntax:
		arrayName.unshift(elementA)
		arrayName.unshift(elementA, elementB)

*/

fruits.unshift("Lime", "Banana");
console.log("Mutated array from the unshift method:");
console.log(fruits);

/*
	shift()
		- removes an element at the beginning of our array and return the removed element.

	Syntax:
		arrayName.shift()
*/

let removedFruit2 = fruits.shift();
console.log(removedFruit2);
console.log("Mutated array from the shift method:");
console.log(fruits);

/*
	splice()
		- allows to simultaneously removes the elements from a specified index number and adds an element.

	Syntax:
		arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);
*/

let fruitsSplice = fruits.splice(1, 2, "Cherry", "Lychee");
console.log("Mutated arrays from splice method:");
console.log(fruits);
console.log(fruitsSplice);

// Using splice without adding elements
let removedSplice = fruits.splice(3, 2);
console.log(fruits);
console.log(removedSplice);

/*
	sort()
		- rearranges the array element in alphanumeric order

	Syntax:
		arrayName.sort();
*/

fruits.sort();
console.log("Mutated array from sort method:");
console.log(fruits);

let mixArr = [12, "May", 36, 94, "August", 5, 6.3, "September", 10, 100, 1000];
console.log(mixArr.sort());

/*
	reverse()
		- reverses the order of the element in an array.

	Syntax:
		arrayName.reverse();
*/

fruits.reverse();
console.log("Mutated array from reverse method:");
console.log(fruits);
mixArr.reverse();
console.log(mixArr);

// for sorting the items in descending order:
fruits.sort().reverse();
console.log(fruits);

	/*MINI ACTIVITY:
	 - Debug the function which will allow us to list fruits in the fruits array.
	 	-- this function should be able to receive a string.
	 	-- determine if the input fruit name already exist in the fruits array.
	 		*** If it does, show an alert message: "Fruit already listed on our inventory".
	 		*** If not, add the new fruit into the fruits array ans show an alert message: "Fruit is not listed in our inventory."
	 	-- invoke and register a new fruit in the fruit array.
	 	-- log the updated fruits array in the console

		*/
// let fruitName;
// function registerFruit(fruitName) {
// 	let doesFruitExist = fruits.includes(fruitName);
// 	if(doesFruitExist) {
// 		alert(fruitName + " is already in our inventory");
// 	} else {
// 		fruits.push(fruitName);
//  		// break;
// 		alert("Fruit is now listed in our inventory")
// 	}
// }

// registerFruit("Papaya");
// console.log(fruits)
// registerFruit("Avocado");
// console.log(fruits);
	 	
// Mini Activity - Array Mutators
/*
	- create an empty array
	- use the push() method and unshift() method to add elements in your array.
	- use the pop() and shift() methods to remove elements in your array.
	- use sort to organize your array in alphanumeric order
	- at the end, the array should contain not less than 6 elements after applying these methods.
	- log the changes in the console every after update
*/

let arrayNumber = ["one", "two", "three", "four", "five"];
console.log(arrayNumber);
let pushNumber = arrayNumber.push("six")
console.log(arrayNumber);
let unshiftNumber = arrayNumber.unshift("zero");
console.log(arrayNumber);
let popNumber = arrayNumber.pop()
console.log(arrayNumber);
let shiftNumber = arrayNumber.shift();
console.log(arrayNumber);
let sortNumber = arrayNumber.sort();
console.log(arrayNumber);

/*
	Non-mutator methods
		- these are functions or methods that do not modify or change an array after they are created.
		- these methods do not manipulate the original array performing various tasks such as returning elements from an array and combining arrays and printing the output.
*/

console.log("");

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];
console.log(countries);

/*
	indexOf()
		- returns the index number of the first matching element found in an array. If no match is found, the result will be -1. The search process will be done from our first element proceeding to the last element.

		Syntax:
			arrayName.indexOf(searchValue);
			arrayName.indexOf(searchValue, fromIndex);
*/

let firstIndex = countries.indexOf("PH");
console.log("Result of indexOf method: " + firstIndex); //1
firstIndex = countries.indexOf("PH", 2);
console.log("Result of indexOf method: " + firstIndex); //5
firstIndex = countries.indexOf("PH", 7);
console.log("Result of indexOf method: " + firstIndex); //-1
firstIndex = countries.indexOf("PH", -1);
console.log("Result of indexOf method: " + firstIndex); //-1

console.log("");
// console.log(typeof(firstIndex));

/*
	lastIndexOf()
		- returns the index number of the last matching element found in an array. The search process will be done from the last element proceeding to the first.

	Syntax:
		arrayName.lastIndexOf(searchValue);
		arrayName.lastIndexOf(searchValue, fromIndex);
*/
let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf method: " + lastIndex);
lastIndex = countries.lastIndexOf("PH", 4);
console.log("Result of lastIndexOf method: " + lastIndex);

/*
	slice()
		- portions / slices elements from our array and returns new array.

	Syntax:
		arrayName.slice(startingIndex, endingIndex);
*/

console.log(countries);
let slicedArrayA = countries.slice(2);
console.log("Result from slice method: " + slicedArrayA);
console.log(countries);

console.log("");
let slicedArrayB = countries.slice(2,5);
console.log("Result from slice method: " + slicedArrayB);
console.log(countries);

console.log("");
let slicedArrayC = countries.slice(-3);
console.log("Result from slice method: " + slicedArrayC);
console.log(countries);

/*
	toString();
		- returns an array as a string separated by commas.
		- is used internally by JS when an array needs to be displayed as text (like in HTML), or when an object/array needs to be used as a string.

	Syntax:
		arrayName.toString();
*/

let stringArray = countries.toString();
console.log("Result from toString method: ");
console.log(stringArray);

/*
	concat()
		- combines two or more arrays and returns the combined results.

	Syntax:
		arrayA.concat(arrayB);
		arrayA.concat(elementA);
*/

let taskArrayA = ["drink HTML", "eat JavaScript"];
let taskArrayB = ["inhale CSS", "breath SASS"];
let taskArrayC = ["get git", "be node"];

let tasks = taskArrayA.concat(taskArrayB);
console.log("Result from concat method: ");
console.log(tasks);

let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
console.log(allTasks);

// Combining arrays with elements - similar with push
let combineTask = taskArrayA.concat("smell express", "throw react");
console.log(combineTask);

/*
	join()
		- returns an array as a string.
		- does not change the original array.
		- we can use any separator. The default is comma(,)

	Syntax:
		arrayName.join(separatorString);
*/

let students = ["Elysha", "Gab", "Ronel", "Jean"];
console.log(students.join());
console.log(students.join(" "));
console.log(students.join(", "));
console.log(students.join(" - "));

// console.log(mixArr.toString().join());

/*
	Iteration methods
		- are loops designed to perform repetitive tasks in an array. used for manipulating array data resulting in complex tasks.
		- normally works with a function supplied as an argument.
		- aims to evaluate each element in an array.
*/
/*
	forEach()
		- similar to for loop that iterates on each array element

	Syntax:
		arrayName.forEach(function(individualElement) {
			statement;
		});
*/

allTasks.forEach(function(task) {
	console.log(task);
})

// Using forEach with conditional statements

let filteredTasks = [];
allTasks.forEach(function(task) {
	if(task.length > 10) {
		filteredTasks.push(task);
	};
});

console.log(allTasks);
console.log("Result of filteredTasks: ");
console.log(filteredTasks);

/*
	map()
		- iterates on each element and returns a new array with different value depending on the results of the function's operation.

	Syntax:
		arrayName.map(function(individualElement) {
			statement;
		});
*/

let numbers = [1,2,3,4,5];

let numberMap = numbers.map(function(number) {
	console.log(number);
	return number*number;
});

console.log("Original array:");
console.log(numbers);
console.log("Result of the map method: ");
console.log(numberMap);

/*
	every()
		- checks if all elements in an array met the given condition. Returns a true value if all elements meet the condition and false if otherwise.

	Syntax:
		arrayName.every(function(individualElement) {
			return expression/condition
		});
*/

let allValid = numbers.every(function(number) {
	return(number > 3);
});

console.log("Result of every method:");
console.log(allValid);

/*
	some()
		- checks if at least one element in the array meet the given condition. Returns a true value if atleast one of the elements meets the given condition and returns false if otherwise.

	Syntax:
		arrayName.some(function(individualElement) {
			return expression/condition;
		});
*/
let someValid = numbers.some(function(number) {
	return(number > 3);
});

console.log("Result from some method:");
console.log(someValid);

/*
	filter()
		- returns a new array that contains elements which meets the given condition. Return an empty array if no elements were found (that satisfies the given condition).

	Syntax:
		arrayName.filter(function(individualElement) {
			return expression/condition;
		});
*/
let filteredValid = numbers.filter(function(number) {
	return (number > 3);
});

console.log("Result of filter method:");
console.log(filteredValid);

/*
	includes()
		- checks if the argument passed can be found in an array
		- can be chained after another method. The result of the first method is used on the second method until all chained methods have been resolved.

	Syntax:
		arrayName.includes()
*/

let products = ["mouse", "KeyBoArd", "laptop", "monitor"];
let filteredProducts = products.filter(function(product) {
	return product.toLowerCase().includes("a");
});

console.log("Results on includes method:");
console.log(filteredProducts);