// console.log("Hi");
/*
    Create functions which can manipulate our arrays.
    You may change the names on the initial list before you use the array varaibles.
*/

let registeredUsers = [

    "James Jeffries",
    "Maggie Williams",
    "Macie West",
    "Michelle Queen",
    "Angelica Smith",
    "Fernando Dela Cruz",
    "Mike Dy"
];

let friendsList = [];

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - Invoke and register a new user in the browser console.
        - In the browser console, log the registeredUsers array.

*/

console.log(registeredUsers);
    
function newUser(name) {
    // let doesNameExist = registeredUsers.includes(name);
    console.log("Register: " + name);
    if (registeredUsers.includes(name)) {
        alert("Registration failed. " + name + " already exists!");
        console.log("Registration failed. " + name + " already exists!");
    } else {
        alert("Thank you for registering! "+ name);
        console.log("Thank you for registering, " + name);
        registeredUsers.push(name);
    };
    
};

newUser("Conan O' Brien");
newUser("Akiko Yukihime");
newUser("Michelle Queen");
console.log("Registered Users:")
console.log(registeredUsers);

/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - This function should be able to receive a string.
        - Determine if the input username exists in our registeredUsers array.
            - If it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - If it is not, show an alert window with the following message:
                - "User not found."
        - Invoke the function and add a registered user in your friendsList in the browser console.
        - In the browser console, log the friendsList array in the console.

*/

function addFriend(friend) {
    console.log("Is " + friend + " your friend?");
    let foundUser = registeredUsers.includes(friend);
    if (foundUser) {
        alert("You have added " + friend + " as a friend!")
        console.log("You have added " + friend + " as a friend!")
        friendsList.push(friend);
    } else {
        alert (friend + " not found");
        console.log(friend + " not found");
    };
    
};

addFriend("James Jeffries");
addFriend("Akiko Yukihime");
addFriend("Shane Miguelito");
addFriend("Macie West");
addFriend("Mike Dy");
console.log("Your friends:")
console.log(friendsList);

/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/

function showFriendsList(){
    if (!friendsList.length){
            alert("You currently have 0 friends. Add one first.");
    } else {
        friendsList.forEach(function(friend){
            console.log(friend);
        });        
    }
}
showFriendsList();

    
// STRETCH GOALS

/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function in the browser console.
*/
console.log("");

function countInFriendlist(){
    if (!friendsList.length){
        alert("You currently have 0 friends. Add one first.");
    } else {
        let nummberOfFriends = friendsList.length;
        alert("You currently have " + nummberOfFriends + " friends.");
        console.log("You currently have " + nummberOfFriends + " friends.")
    }
}
countInFriendlist();

/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - In the browser console, log the friendsList array.

*/
console.log("");

function deleteFriend() {
    if(!friendsList.length){
        alert("You currently have 0 friends. Add one first.");
    } else {
        friendsList.pop();
    }
}
showFriendsList();
console.log("");
deleteFriend();
console.log("Your current friend list after delete last user:");
showFriendsList();


/*======================================================================================*/



// Try this for fun:
/*
   

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/
console.log("");
function deleteFriendBySplice(startIndex, deleteCount){
    if(!friendsList.length){
        alert("You currently have 0 friends. Add one first.");
    } else {
        friendsList.splice(startIndex, deleteCount);
    }    
}
// showFriendsList()
// console.log("");
deleteFriendBySplice(1,1)
console.log("Your current friends list after deleting specific user by index");
showFriendsList()